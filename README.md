# READ.ME #

* This is for candidates going through the interview process for the junior web developer position at Eldon Insurance Services Ltd

### What is this repository for? ###

* This is a repo set up for junior developers to download a theme folder and a design and to follow the instructions provided
* Once you have completed the tasks assigned to you, please zip up your theme folder and a MySQL dump of the database you were working with in your local environment.

### INSTRUCTIONS PROVIDED SEPERATELY ###

* Version 1.0.0

### HAPPY CODING :) ###